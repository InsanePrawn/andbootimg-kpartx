#!/usr/bin/env python3
"""Kpartx-like functionality for android boot.img images"""

# "Inspired by" https://android.googlesource.com/platform/system/tools/mkbootimg/+/refs/heads/master/unpack_bootimg.py
# Released under the i-dont-know-if-i-can-gpl-this-but-try-to-keep-it-open-source-pls license v1.

# Header spec: https://android.googlesource.com/platform/system/tools/mkbootimg/+/refs/heads/master/include/bootimg/bootimg.h


from argparse import ArgumentParser, FileType
from struct import unpack
import sys
import subprocess
import json

BOOT_IMAGE_HEADER_V3_PAGESIZE = 4096
VENDOR_RAMDISK_NAME_SIZE = 32
VENDOR_RAMDISK_TABLE_ENTRY_BOARD_ID_SIZE = 16

class DictDefaulter(json.JSONEncoder):
    def default(self, o):
            return o.__dict__

class Segment:
#    offset = -1
    # what the segment's length field tells us if available
#    length_advertised = 0
    # what the distance to the next known segment actually is.
#    distance_next = -1

    def __init__(self, offset=-1, length_advertised=0, distance_next=-1):
        self.offset = offset
        self.length_advertised = length_advertised
        self.distance_next = distance_next

    def _dict_repr(self):
        dict_repr = {'addr': f'{self.offset} ({hex(self.offset)})', 'len': f'{self.length_advertised} ({hex(self.length_advertised)})'}
        if self.distance_next >= 0:
            dict_repr.update({'dist': self.distance_next})
        return dict_repr

    def __str__(self):
        return 'seg:' + str( self._dict_repr() )
    def __repr__(self):
        return 'seg:' + repr( self._dict_repr() )

class HeaderInfo:
    magic = None
    version = -1
    page_size = None
    segments = None
    infos = {}
    def __str__(self):
        return str(self.__dict__)
    def __repr__(self):
        return repr(self.__dict__)

def get_number_of_pages(image_size, page_size):
    """calculates the number of pages required for the image"""
    return (image_size + page_size - 1) // page_size

def cstr(s):
    """Remove first NULL character and any character beyond."""
    return s.split('\0', 1)[0]

def parse_header(header_stream):
    """ Parse the leading header bytes from the input stream. May be passed a full image. """
    result = HeaderInfo()
    bmagic = unpack('8s', header_stream.read(8))[0]
    if bmagic == b"VNDRBOOT":
        print('Vendor boot format not implemented yet, aborting parse. Sorry!')
        result.magic = "VNDRBOOT"
        return result
    if bmagic != b"ANDROID!":
        raise Exception('Unknown boot magic: ' + repr(result.magic) + ' (Wrong file format?)')
    result.magic = bmagic.decode()

    # First chunk!
    main_info_raw = unpack('9I', header_stream.read(9 * 4))
    result.version = main_info_raw[8]
    result.segments = {'kernel': None, 'ramdisk': None, 'second': None, }

    if result.version < 3:
        result.segments['kernel'] = Segment( length_advertised=main_info_raw[0],
            offset=main_info_raw[1]
        )
        result.segments['ramdisk'] = Segment(
            length_advertised=main_info_raw[2],
            offset=main_info_raw[3]
        )
        result.segments['second'] = Segment(
            length_advertised=main_info_raw[4],
            offset=main_info_raw[5]
        )
        result.segments['kernel_tags'] = Segment(
            offset=main_info_raw[6]
        )
        result.page_size = main_info_raw[7]
        result.patch_level_raw = unpack('I', header_stream.read(1 * 4))[0]
    else:
        result.page_size = BOOT_IMAGE_HEADER_V3_PAGESIZE
        num_header_pages = 1
        num_kernel_pages = get_number_of_pages(kernel_size, page_size)
        result.segments['kernel'] = Segment(
            length_advertised=main_info_raw[0],
            offset=result.page_size * num_header_pages
        )
        result.segments['ramdisk'] = Segment(
            length_advertised=main_info_raw[2],
            offset= (num_header_pages + num_kernel_pages) * result.page_size
        )
        result.patch_level_raw = main_info_raw[2]

    # Second chunk!
    if result.version < 3:
        result.infos['product_name'] = cstr(
            unpack('16s', header_stream.read(16))[0].decode())
        result.infos['cmdline'] = cstr(
            unpack('512s', header_stream.read(512))[0].decode())
    else:
        result.infos['cmdline'] = cstr(
            unpack('1536s', header_stream.read(1536))[0].decode())
        mkbootimg_args = get_boot_image_v3_args(
            result.version,
            main_info_raw[2],  # os_version and patch_level
            cmdline)

    # Third chunk!
    if result.version < 3:
        header_stream.read(32)  # ignore SHA
        result.infos['extra_cmdline'] = cstr(
            unpack('1024s', header_stream.read(1024))[0].decode())
    if 0 < result.version < 3:

        result.segments['recovery_dtbo'] = Segment(
            length_advertised=unpack('I', header_stream.read(1 * 4))[0],
            offset=unpack('Q', header_stream.read(8))[0]
        )

        result.segments['recovery_dtbo'] = Segment(
            length_advertised=unpack('I', header_stream.read(1 * 4))[0],
            offset=unpack('Q', header_stream.read(8))[0]
        )

        result.segments['boot_header'] = Segment(
            length_advertised=unpack('I', header_stream.read(1 * 4))[0],
            offset=-1  # TODO: investigate?
        )
    # else:
    #    recovery_dtbo_size = 0
    if 1 < result.version < 3:
        result.segments['dtb'] = Segment(
            length_advertised=unpack('I', header_stream.read(4))[0],
            offset=unpack('Q', header_stream.read(8))[0]
        )
    # else:
    #    dtb_size = 0

    return result


def parse_args(args=None):
    parser = ArgumentParser(
        description='Kpartx-like functionality for Android boot images. Analyzes segments and provides symlinked device mappings'
    )
    parser.add_argument('image_path', metavar='IMGFILE', help='path to the boot image')
    parser.add_argument('--json', help="Dumps the parsed header infos as JSON", action='store_true', default=False)
    parser.add_argument('--prefix', help="The prefix of the device mappings to create. Default: androidboot", default='androidboot')
    actions = parser.add_mutually_exclusive_group()
    actions.add_argument(
        '-a', '--add-mappings', help='Create the device mappings for the image', action='store_true', default=False)
    actions.add_argument(
        '-r', '--remove-mappings', help='Remove open device mappings for the image', action='store_true', default=False)
    #TODO:
    #actions.add_argument(
    #    '--dump-contents', help="Dumps the segments' contents to files", action='store_true', default=False)
    # TODO: add filter for things to be mapped (kernel, initramfs, dtb, etc.)
    return parser.parse_args(args)

def create_loop_dev(file, segment):
    args = ['losetup', '--show', '--offset', str(segment.offset // 8), '--find', file]
    longest = max(segment.length_advertised, segment.distance_next)
    if longest > 0:
        args += ['--sizelimit', str(longest)]
    #print(repr(args))
    result = subprocess.run(args, stdout=subprocess.PIPE)
    # TODO: error handling?
    return (result.stdout.decode().strip(), result)

def create_loop_mapping(file, segment, name, prefix='androidboot'):
    loopdev_setup = create_loop_dev(file, segment)
    if loopdev_setup[1].returncode != 0:
        return (loopdev_setup, None)
    symlink_path = '/dev/mapper/%s-%s' % (prefix, name)
    symlink_setup = subprocess.run(['ln', '-v', '-s', '-f', loopdev_setup[0], symlink_path])
    return (loopdev_setup, (symlink_path, symlink_setup))


def main(args=None):
    global header
    global offsets_sorted
    args_parsed = parse_args(args)
    img_stream = open(args_parsed.image_path, 'rb')
    header = parse_header(img_stream)

    if args_parsed.json:
        print(json.dumps(header, cls=DictDefaulter))
    else:
        print(repr(header))

    if args_parsed.add_mappings:
        offset_segments = {}
        for seg_name, segment in header.segments.items():
            if segment.offset > -1:
                offset_segments[segment.offset] = (seg_name, segment)

        segments_sorted = []
        offsets_sorted = list(offset_segments.keys())
        offsets_sorted.sort()
        for i, offset in enumerate(offsets_sorted):
            segment = offset_segments[offset][1]
            if i < len(offsets_sorted) - 1:
                next_offset = offsets_sorted[i+1]
                segment.distance_next = next_offset - segment.offset
            if segment.length_advertised > 0:
                segments_sorted.append(offset_segments[offset])

        print(segments_sorted)
        for seg_name, segment in segments_sorted:
            create_loop_mapping(file=args_parsed.image_path, segment=segment, name=seg_name, prefix=args_parsed.prefix)


if __name__ == '__main__':
    main()
